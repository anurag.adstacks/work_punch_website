package com.work_punch_website.service;

import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.*;
import java.util.Date;
import java.util.Properties;

@Service
public class SendMailService {
    public void sendMessage(String fullName, String emailAddress, String message, String subject) {
        try {
            // Get system properties
            Properties props = new Properties();
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", "mail.adstacks.in");
            props.put("mail.smtp.port", "587");
            props.put("mail.smtp.socketFactory.fallback", "true");

            Session session = Session.getInstance(props, new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication("support@work-punch.com", "Work-Punch@739");
                }
            });
            Message msg = new MimeMessage(session);
            // To get the array of to addresses
            msg.addRecipient(Message.RecipientType.TO, new InternetAddress("support@work-punch.com"));
            msg.setFrom(new InternetAddress("support@work-punch.com", false));
            msg.setSubject(subject);
            msg.setSentDate(new Date());
            MimeBodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent("<p>Name : - "+fullName+"</p><p>Email : - "+emailAddress+"</p><p>"+message+"</p>", "text/html");
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart);
            msg.setContent(multipart);
            Transport.send(msg);

        } catch (MessagingException e) {
            System.out.println("535 Incorrect authentication data");
        }
    }
}
