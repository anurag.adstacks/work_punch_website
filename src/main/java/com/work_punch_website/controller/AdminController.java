package com.work_punch_website.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AdminController {
     @GetMapping("/")
     public String home() {
         return "home";
     }
    @GetMapping("/home")
    public String home1() {
        return "home";
    }
    @GetMapping("/hrPlatform")
      public String hrPlatform(){
        return "hrPlatform";
    }
    @GetMapping("/employeePlatform")
    public String employeePlatform(){
        return "employeePlatform";
    }
    @GetMapping("/employeeRecord")
    public String employeeRecord(){
        return "employeeRecord";
    }
    @GetMapping("/attendanceManagement")
    public String attendanceManagement(){
        return "attendanceManagement";
    }
    @GetMapping("/leaveReport")
    public String leaveReport(){
        return "leaveReport";
    }
    @GetMapping("/monthlySummary")
    public String monthlySummary(){
        return "monthlySummary";
    }
    @GetMapping("/recruitmentAndOnboarding")
    public String recruitmentAndOnboarding(){
        return "recruitmentAndOnboarding";
    }
    @GetMapping("/addNotes")
    public String addNotes(){
        return "addNotes";
    }
    @GetMapping("/addFiles")
    public String addFiles(){
        return "addFiles";
    }
    @GetMapping("/taskManagement")
    public String taskManagement(){
        return "taskManagement";
    }
    @GetMapping("/applyingLeave")
    public String applyingLeave(){
        return "applyingLeave";
    }
    @GetMapping("/resources")
    public String resources(){
        return "resources";
    }
    @GetMapping("/contactUs")
    public String contactUs(){
        return "contactUs";
    }
}

