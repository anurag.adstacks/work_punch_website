package com.work_punch_website.controller;

import com.work_punch_website.service.SendMailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("mail")
public class EmailController {
    @Autowired
    SendMailService sendMailService;
    @RequestMapping(value = "/sendMessage", method = RequestMethod.POST)
    public ResponseEntity<?> sendMessage(@RequestParam(value = "fullName") String fullName,
                                         @RequestParam(value = "emailAddress") String emailAddress,
                                         @RequestParam(value = "message") String message,
                                         @RequestParam(value = "subject") String subject){
        System.out.println("fullName = "+fullName);
        System.out.println("emailAddress = "+emailAddress);
        System.out.println("message = "+message);
        sendMailService.sendMessage(fullName, emailAddress, message, subject);

        return ResponseEntity.ok().body("Send Successfully");
    }
}
