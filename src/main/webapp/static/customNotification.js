function showNotification(from, align,message) {

    $.notify({
      icon: "add_alert",
      message: message,

    }, {
      type: 'warning',
      timer: 3000,
      placement: {
        from: from,
        align: align
      }
    });
  }