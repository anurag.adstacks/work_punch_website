<!DOCTYPE html>
<html lang="en">
<head>
    <!-- basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- mobile metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="icon" type="image/png" href="..\assets\images\work-punch-logo.png">
    <!-- site metas -->
    <title>Work Punch </title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- bootstrap css -->
    <link rel="stylesheet" type="text/css" href="..\assets\css\bootstrap.min.css">
    <!-- style css -->
    <link rel="stylesheet" type="text/css" href="..\assets\css\style.css">
    <!-- Responsive-->
    <link rel="stylesheet" href="..\assets\css\responsive.css">
    <!-- fevicon -->
    <link rel="icon" href="..\assets\images\fevicon.png" type="image/gif"/>
    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="..\assets\css\jquery.mCustomScrollbar.min.css">
    <!-- Tweaks for older IEs-->
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
    <!-- owl stylesheets -->
    <link rel="stylesheet" href="..\assets\css\owl.carousel.min.css">
    <%--<link rel="stylesheet" href="..\assets\css\owl.theme.default.min.css">--%>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css"
          media="screen">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">


    <style>
        .material-symbols-outlined {

            font-size: 50px;
        }
    </style>
</head>
<body>
<jsp:include page="header.jsp">
    <jsp:param value="active" name="ourPlatform"/>
    <jsp:param value="active" name="monthlySummary"/>
</jsp:include>

<div class="section-heading  p-lg-5 ">
    <div class="row layout_padding">
        <div class="col-md-6">
            <h1 class="text-center">Highlights and Insights for the Month</h1>
            <p>Our monthly summary provides a comprehensive overview of the key highlights and insights from the past
                month, offering valuable information to stakeholders and decision-makers. This report serves as a
                valuable tool for understanding the progress made, identifying significant achievements, and gaining
                insights into the overall performance of the organization.</p>
            <p>This monthly summary report serves as a valuable tool for tracking employee performance, monitoring attendance, and facilitating communication between employees and their supervisors. It also helps in identifying areas for improvement and setting goals for future development.</p>
        </div>
        <div class="col-md-6 shadow-sm ">
            <img src="..\assets\images\monthly-report-image-660x438.jpg" atl="">

        </div>
    </div>
</div>


<div class="header_section  layout_padding">
    <div class="row p-5 " style="height:auto">
        <div class="col-md-12">
            <h2 class="text-center" style="color:black;">
                The monthly summary feature of our software enhances productivity by consolidating crucial information,
                enabling users to stay informed and take action.</h2>

        </div>

    </div>
</div>

<div class="row p-5">
    <div class="col-md-6 ">
        <div class="text-center">
            <img src="..\assets\images\summaryReport.webp" alt="">
        </div>
    </div>
    <div class="col-md-6 text-left mt-lg-5">
        <h1 class="text-center">Monthly Summary</h1>
        <p class="center" style="text-align: center">Our Work Punch monthly summary provides a consolidated overview of
            employee attendance and work hours, enabling efficient tracking and analysis of workforce productivity</p>
        <p class="center" style="text-align: center">A monthly summary of an employee's tasks, attendance, leave, and other relevant information provides a comprehensive overview of their performance and activities during a specific month.</p>
    </div>
</div>



<div class="client_section p-5" style="height: auto">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="bottom_heading text-center">Monthly Summary Report</h4>
                <p style="text-align: center">This monthly summary report serves as a valuable tool for tracking employee performance, monitoring attendance, and facilitating communication between employees and their supervisors. It also helps in identifying areas for improvement and setting goals for future development.</p>
            </div>
        </div>
    </div>
</div>

<!-- footer section start -->
<jsp:include page="footer.jsp" />


<%--jsScript--%>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js">
</script>

<script>
    function hrPlatform() {
        window.open('/hrPlatform','_self');
    }

    function empPlatform(){
        window.open('/employeePlatform','_self')
    }

</script>
</body>
</html>