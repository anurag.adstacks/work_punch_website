<!DOCTYPE html>
<html lang="en">
<head>
   <!-- basic -->
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <!-- mobile metas -->
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <meta name="viewport" content="initial-scale=1, maximum-scale=1">
   <link rel="icon" type="image/png" href="..\assets\images\work-punch-logo.png">
   <!-- site metas -->
   <title>Work Punch </title>
   <meta name="keywords" content="">
   <meta name="description" content="">
   <meta name="author" content="">
   <!-- bootstrap css -->
   <link rel="stylesheet" type="text/css" href="..\assets\css\bootstrap.min.css">
   <!-- style css -->
   <link rel="stylesheet" type="text/css" href="..\assets\css\style.css">
   <!-- Responsive-->
   <link rel="stylesheet" href="..\assets\css\responsive.css">
   <!-- fevicon -->
   <link rel="icon" href="..\assets\images\fevicon.png" type="image/gif"/>
   <!-- Scrollbar Custom CSS -->
   <link rel="stylesheet" href="..\assets\css\jquery.mCustomScrollbar.min.css">
   <!-- Tweaks for older IEs-->
   <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
   <!-- owl stylesheets -->
   <%--<link rel="stylesheet" href="..\assets\css\owl.carousel.min.css">
   <link rel="stylesheet" href="..\assets\css\owl.theme.default.min.css">--%>
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css"
         media="screen">
   <link rel="stylesheet"
         href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0"/>

   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">

</head>
<body>
<!--header section start -->
<jsp:include page="header.jsp">
   <jsp:param value="active" name="contactUs"/>
</jsp:include>
<br><br><br>
<div class="client_section p-5" style="height: auto;background: linear-gradient(to top left, #78CCDB 0%,  #4EA5FF 100%);">
   <div class="container">
      <div class="row" style="margin-top: 2%">
         <div class="col-sm-12">
            <h1 class="bottom_heading text-center" style="color: #FFFFFF">Contact Us</h1>
            </p>
         </div>
      </div>
   </div>
</div>
<div class=container-fluid" id="gtco-footer">
  <%-- <form id="saveBenefits" class="form-horizontal">--%>
<div class="row p-xl-5"  >
   <div class="col-md-12 text-center" id="contact">
      <h2><u style="text-underline-offset: 10px;">Get In Touch</u> </h2>
      <div style="margin-top: 20px">
      <input type="text" class="form-control" id="fullName" placeholder="Full Name*" style=""><br>
      <input type="email" class="form-control" id="emailAddress" placeholder="Email Address*"><br>
      <input type="text" class="form-control" id="subject" placeholder="Subject*"><br>
      <textarea class="form-control" id="message" placeholder="Message*"></textarea><br>
      <button class="submit-button text-center" id="sendMessage" onclick="sendMessage()">Send Message1</button>
      </div>
   </div>
</div>
   <%--</form>--%>
   <div class="footer-cta pt-xl-5 " style="background-color: #ADD8E6;">
      <div class="row p-xl-5 mt-xl-5" style="margin-left: 8%;margin-top: -30px !important;">
         <div class="col-xl-4 col-md-4 ">
            <div class="single-cta">
               <i class="fas fa-map-marker-alt"></i>
               <div class="cta-text">
                  <h4 style="margin-top: 8px">Find us</h4>
                  <p style="color: #000000">Gurugram, Haryana 122001</p>
               </div>
            </div>
         </div>
         <div class="col-xl-1 col-md-1"></div>
         <div class="col-xl-4 col-md-4">
            <div class="single-cta">
               <i class="fas fa-phone"></i>
               <div class="cta-text">
                  <h4 style="margin-top: 6px">Call us</h4>
                  <span style="color: #000000">08800336578</span>
               </div>
            </div>
         </div>
         <div class="col-xl-3 col-md-3">
            <div class="single-cta">
               <i class="far fa-envelope-open"></i>
               <div class="cta-text">
                  <h4 style="margin-top: 10px">Mail us</h4>
                  <span><a href="/contactUs">support@work-punch.com</a></span>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


<!-- footer section start -->
<jsp:include page="footer.jsp" />


<!-- copyright section end -->
<!-- Javascript files-->
<script src="..\assets\js\jquery.min.js"></script>
<script src="..\assets\js\popper.min.js"></script>
<script src="..\assets\js\bootstrap.bundle.min.js"></script>
<%--<script src="..\assets\js\jquery-3.0.0.min.js"></script>--%>
<script src="..\assets\js\plugin.js"></script>
<!-- sidebar -->
<script src="..\assets\js\jquery.mCustomScrollbar.concat.min.js"></script>
<script src="..\assets\js\custom.js"></script>
<%--<script src="..\assets\js\clickButton.js"></script>--%>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
   function hrPlatform() {
      window.open('/hrPlatform', '_self');
   }

   function empPlatform() {
      window.open('/employeePlatform', '_self')
   }

   function sendMessage(){
      var fullName=document.getElementById("fullName").value;
      var emailAddress=document.getElementById("emailAddress").value;
      var message=document.getElementById("message").value;
      var subject=document.getElementById("subject").value;
      /*$("#sendMessage").prop('disabled', true);*/
      event.preventDefault();
      if(fullName.replace(/\s/g,'')!=='' && emailAddress.replace(/\s/g,'')!=="" && message.replace(/\s/g,'')!==''
      && subject.replace(/\s/g,'')!=='') {
         $.ajax({
            url: "/mail/sendMessage?fullName=" + fullName + "&emailAddress=" + emailAddress + "&message=" + message+"&subject="+subject,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            beforeSend: function () {


            },
            success: function (response) {
               /*showNotification('top', 'left', 'Your application has been submitted successfully');*/
               Swal.fire({
                  position: "top-center",
                  icon: "success",
                  title: response,
                  showConfirmButton: false,
                  timer: 1500
               });
               /*location.reload();*/
               setTimeout(function () {
                  window.location.reload();
               }, 1000);
            },
            complete: function () {
               // Hide image container

            },

            error: function (jqXHR) {
               /*var o = $.parseJSON(jqXHR.responseText);
               window.open('/adminConsole/error?status=' + o.status + '&message=' + o.error, "_self")*/
            }
         });
      }

   }



</script>
</body>
</html>