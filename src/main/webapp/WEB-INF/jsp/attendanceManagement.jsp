<!DOCTYPE html>
<html lang="en">
<head>
    <!-- basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- mobile metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="icon" type="image/png" href="..\assets\images\work-punch-logo.png">
    <!-- site metas -->
    <title>Work Punch </title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- bootstrap css -->
    <link rel="stylesheet" type="text/css" href="..\assets\css\bootstrap.min.css">
    <!-- style css -->
    <link rel="stylesheet" type="text/css" href="..\assets\css\style.css">
    <!-- Responsive-->
    <link rel="stylesheet" href="..\assets\css\responsive.css">
    <!-- fevicon -->
    <link rel="icon" href="..\assets\images\fevicon.png" type="image/gif"/>
    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="..\assets\css\jquery.mCustomScrollbar.min.css">
    <!-- Tweaks for older IEs-->
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
    <!-- owl stylesheets -->
    <link rel="stylesheet" href="..\assets\css\owl.carousel.min.css">
    <%--<link rel="stylesheet" href="..\assets\css\owl.theme.default.min.css">--%>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css"
          media="screen">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0"/>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">

</head>
<body>
<!--header section start -->
<jsp:include page="header.jsp">
    <jsp:param value="active" name="ourPlatform"/>
    <jsp:param value="active" name="attendanceManagement"/>
</jsp:include>
<div class="section-heading p-sm-5">
    <div class="row layout_padding">
        <div class="col-md-6  ">
            <h1 class="text-center">Automate Attendance Tracking and Analysis with Workpunch</h1>
            <p class="text-center">
                Workpunch offers a comprehensive attendance management solution that revolutionizes the way
                organizations track and analyze employee attendance. With Workpunch, the cumbersome manual processes of
                attendance tracking are eliminated, and automation takes over. By leveraging advanced technologies,
                Workpunch simplifies the entire process, saving time and effort for HR teams.</p>
        </div>
        <div class="col-md-6 shadow-sm ">
            <img src="..\assets\images\download.webp" atl="">

        </div>
    </div>
</div>
<div class="header_section mt-xl-1 layout_padding">
    <div class="row p-5 " style="height:auto">
        <div class="col-md-1">

        </div>
        <div class="col-md-11">
            <h1 class="text-center" style="color:#000000;">Optimize Workforce Attendance with Innovative Tracking
                Systems</h1>
            <p class="text-center mt-sm-3">Efficient attendance management is crucial for
                accurately tracking and monitoring the workforce, ensuring productivity, and maintaining
                compliance.</p>
        </div>

    </div>
</div>


<section id="gallery ">
    <div class="container p-xl-5">
        <div class="row">
            <div class="col-lg-4 mb-4 margin_top90">
                <div class="card">
                    <img src="..\assets\images\leaveRequest.jpg" alt="" class="card-img-top">
                    <div class="card-body">
                        <h5 class="card-title text-center">Leave Request Management</h5>
                        <p class="card-text">Leave management refers to the process of managing and tracking employee absences from work, including various types of leaves such as vacation, sick leave, personal leave, and maternity/paternity leave. The primary goal of leave management is to ensure that employees take leave in accordance with company policies while minimizing disruptions to workflow and maintaining productivity. </p>

                    </div>
                </div>
            </div>
            <div class="col-lg-4 mb-4 margin_top90">
                <div class="card">
                    <img src="..\assets\images\lateComingErlyLeaving.png" alt="" class="card-img-top">
                    <div class="card-body" style="margin-top: 30px">
                        <h4 class="card-title text-center">Flexible Work Hours: Embracing Early Leave and Late
                            Coming</h4>
                        <p class="card-text">
                            Employees have been leaving the office premises earlier than the designated departure time,
                            which disrupts workflow and affects overall productivity. It is essential for all employees
                            to adhere to the scheduled working hours and seek necessary permissions when unforeseen
                            circumstances require early departure.
                        </p><br>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 mb-4 margin_top90">
                <div class="card">
                    <img src="..\assets\images\attendances.jpg" alt="" class="card-img-top" style="height: 190px">
                    <div class="card-body" style="margin-top: 30px">
                        <h5 class="card-title text-center">Daily Attendance Report</h5>
                        <p class="card-text">
                            The daily attendance report provides a summary of employee attendance for a specific day,
                            capturing details such as the number of employees present, absent, and any instances of late
                            arrival or early departure. This report helps track and monitor employee attendance
                            patterns, facilitating effective workforce management and ensuring compliance with
                            organizational policies.</p><br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="client_section p-5" style="height: auto">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="bottom_heading text-center">Revolutionize your attendance management with Workpunch</h4>
                <p style="text-align: center">The cutting-edge solution that automates tracking, simplifies analysis, and enhances productivity for
                    your workforce. Visit our website to discover how Workpunch can transform your attendance processes
                    and drive efficiency in your organization.</p>
            </div>
        </div>
    </div>
</div>
<!-- footer section start -->
<jsp:include page="footer.jsp" />


<%--jsScript--%>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js">
</script>

<script>
    function hrPlatform() {
        window.open('/hrPlatform','_self');
    }

    function empPlatform(){
        window.open('/employeePlatform','_self')
    }

</script>
</body>
</html>