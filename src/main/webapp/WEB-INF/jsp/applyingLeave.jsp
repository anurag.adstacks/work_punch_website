<!DOCTYPE html>
<html lang="en">
<head>
    <!-- basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- mobile metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="icon" type="image/png" href="..\assets\images\work-punch-logo.png">
    <!-- site metas -->
    <title>Work Punch </title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- bootstrap css -->
    <link rel="stylesheet" type="text/css" href="..\assets\css\bootstrap.min.css">
    <!-- style css -->
    <link rel="stylesheet" type="text/css" href="..\assets\css\style.css">
    <!-- Responsive-->
    <link rel="stylesheet" href="..\assets\css\responsive.css">
    <!-- fevicon -->
    <link rel="icon" href="..\assets\images\fevicon.png" type="image/gif"/>
    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="..\assets\css\jquery.mCustomScrollbar.min.css">
    <!-- Tweaks for older IEs-->
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
    <!-- owl stylesheets -->
    <link rel="stylesheet" href="..\assets\css\owl.carousel.min.css">
    <%--<link rel="stylesheet" href="..\assets\css\owl.theme.default.min.css">--%>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css"
          media="screen">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0"/>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">

</head>
<body>
<!--header section start -->
<!--header section start -->
<jsp:include page="header.jsp">
    <jsp:param value="active" name="ourPlatform"/>
    <jsp:param value="active" name="leaveApplying"/>
</jsp:include>
<br>
<div class="banner_container p-xl-5">
    <div class="row mt-xl-5">
        <h1 style="width: 100%;text-align: center">Effortless Leave Management for Employees on
            WorkPunch</h1>
        <p class="text-center">
            WorkPunch offers employees a seamless and effortless leave management experience. With our user-friendly
            platform, applying for leave becomes a hassle-free process. We understand the importance of a healthy
            work-life balance, and that's why we have designed our system to simplify time off management for
            employees.</p>
    </div>
</div>
<div class="row p-xl-5">
    <div class="col-md-6 p-xl-5">
        <div class="img_cont shadow-lg">
            <img src="../assets/images/leaveImg.jpg" alt="leaveapply">
        </div>
    </div>
    <div class="col-md-6 mt-xl-5">
        <h2 class="intro_heading text-center mt-xl-5">
            Empower Yourself, Apply for Leave in a Few Simple Steps
        </h2>
        <p><b style="font-weight: bold">Know your company's leave policy:</b> Familiarize yourself with your organization's leave policy, including the types of leave available (e.g., annual leave, sick leave, unpaid leave) and the procedures for applying for leave.</p>
        <p><b style="font-weight: bold">Check your leave balance:</b> Before applying for leave, ensure you have enough accrued leave days available. This information is typically available through your company's HR portal or by consulting with your HR department.</p>
        <p><b style="font-weight: bold">Select the appropriate leave type:</b> Determine the type of leave that best suits your situation (e.g., annual leave for planned vacations, sick leave for illness or medical appointments, etc.).</p>
        <p><b style="font-weight: bold">Choose your leave dates:</b> Decide on the dates you wish to take leave. Consider factors such as workload, project deadlines, and any prior commitments when selecting your leave dates.</p>
        <p><b style="font-weight: bold">Submit your leave application:</b> Follow the designated process for submitting your leave application. This may involve filling out a form, sending an email to HR or your supervisor, or using an online leave management system.</p>
    </div>
</div>

<div class="client_section p-5" style="height: auto">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="bottom_heading text-center">Communication and Notifications</h4>
                <p style="text-align: center">Notifying employees and managers about the status of leave requests, including approvals, denials, and any changes to the requested leave dates. This helps ensure transparency and keeps everyone informed.
                </p>
            </div>
        </div>
    </div>
</div>
<!-- footer section start -->
<jsp:include page="footer.jsp" />



<!-- Javascript files-->
<script src="..\assets\js\jquery.min.js"></script>
<script src="..\assets\js\popper.min.js"></script>
<script src="..\assets\js\bootstrap.bundle.min.js"></script>
<%--<script src="..\assets\js\jquery-3.0.0.min.js"></script>--%>
<script src="..\assets\js\plugin.js"></script>
<!-- sidebar -->
<script src="..\assets\js\jquery.mCustomScrollbar.concat.min.js"></script>
<script src="..\assets\js\custom.js"></script>

<%--<script src="..\assets\js\clickButton.js"></script>--%>
<script>
    function hrPlatform() {
        window.open('/hrPlatform', '_self');
    }

    function empPlatform() {
        window.open('/employeePlatform', '_self')
    }

</script>
</body>
</html>