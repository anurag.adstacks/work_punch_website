<!DOCTYPE html>
<html lang="en">
<head>
    <!-- basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- mobile metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="icon" type="image/png" href="..\assets\images\work-punch-logo.png">
    <!-- site metas -->
    <title>Work Punch </title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- bootstrap css -->
    <link rel="stylesheet" type="text/css" href="..\assets\css\bootstrap.min.css">
    <!-- style css -->
    <link rel="stylesheet" type="text/css" href="..\assets\css\style.css">
    <!-- Responsive-->
    <link rel="stylesheet" href="..\assets\css\responsive.css">
    <!-- fevicon -->
    <link rel="icon" href="..\assets\images\fevicon.png" type="image/gif"/>
    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="..\assets\css\jquery.mCustomScrollbar.min.css">
    <!-- Tweaks for older IEs-->
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
    <!-- owl stylesheets -->
    <link rel="stylesheet" href="..\assets\css\owl.carousel.min.css">
    <%--<link rel="stylesheet" href="..\assets\css\owl.theme.default.min.css">--%>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css"
          media="screen">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0"/>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">


    <style>
        .material-symbols-outlined {

            font-size: 50px;
        }
    </style>
</head>
<body>
<!--header section start -->
<jsp:include page="header.jsp">
    <jsp:param value="active" name="ourPlatform"/>
    <jsp:param value="active" name="leaveReport"/>
</jsp:include>

<div class="section-heading  p-lg-5 ">
    <div class="row layout_padding">
        <div class="col-md-6">
            <h1 class="text-center">Insights into Employee Leave Balances</h1>
            <p>Our comprehensive leave report provides valuable insights into employee leave balances, allowing you to
                effectively manage and monitor time-off usage within your organization. With this report, you can gain a
                clear understanding of the leave entitlements, accruals, and balances of your employees, empowering you
                to make informed decisions and ensure optimal workforce planning.</p>
            <p>Accurate Tracking: By analyzing leave balances, organizations can accurately track how much leave each employee has accumulated, used, and has remaining. This ensures compliance with company policies and labor laws regarding leave entitlements.</p>
            <p>Leave Planning: Employees can use insights into their leave balances to plan their time off effectively. They can make informed decisions about when to take leave based on their available balances and the needs of their teams and projects.</p>
        </div>
        <div class="col-md-6 shadow-sm ">
            <img src="..\assets\images\leave-management-process.png" atl="">

        </div>
    </div>
</div>


<div class="header_section  layout_padding">
    <div class="row p-5 " style="height:auto">
        <div class="col-md-1">

        </div>
        <div class="col-md-11">
            <h2 class="text-center" style="color:#000000;">
                Access our leave report to effortlessly track and manage employee absences, streamlining your leave
                management process.</h2>
        </div>

    </div>
</div>

<div class="row p-5">
    <div class="col-md-6 ">
        <div class="text-center">
            <img src="..\assets\images\portalLeaveApplying.webp" alt="">
        </div>
    </div>
    <div class="col-md-6 text-left mt-lg-5">
        <h1 class="text-center">Leave Report</h1>
        <p class="center" style="text-align: center">The leave report empowers employees to conveniently apply for leave, providing a user-friendly
            interface to request time off, specify the leave type, and select the desired dates, ensuring a seamless and
            efficient process for managing their absences.</p>
        <p style="text-align: center">Leave management refers to the process of managing and tracking employee absences from work, including various types of leaves such as vacation, sick leave, personal leave, and maternity/paternity leave. The primary goal of leave management is to ensure that employees take leave in accordance with company policies while minimizing disruptions to workflow and maintaining productivity.
        </p>
    </div>
</div>


<%--<section id="gallery ">--%>
<%--    <div class="container p-xl-5">--%>
<%--        <div class="row">--%>
<%--            <div class="col-lg-6 mb-4 margin_top90">--%>
<%--                <div class="card shadow-sm">--%>
<%--                    <div class="card-body">--%>
<%--            <span class="material-symbols-outlined" style="color:purple;">--%>
<%--demography--%>
<%--</span>--%>
<%--                        <h3 class="card-title mt-3 text-center">College Data Integration for Streamlined HR--%>
<%--                            Operations</h3>--%>
<%--                        <p class="mt-2">In the dynamic realm of higher education HR, the seamless integration of college--%>
<%--                            data is--%>
<%--                            instrumental in optimizing operations and driving efficiency. By harnessing the power of--%>
<%--                            college data within our HRMS portal, we enable institutions to streamline their HR--%>
<%--                            operations and make informed decisions with valuable insights.</p>--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--            </div>--%>

<%--            <div class="col-lg-6 mb-4 margin_top90">--%>
<%--                <div class="card shadow-sm">--%>
<%--                    <div class="card-body">--%>
<%--          <span class="material-symbols-outlined" style="color:lightseagreen">--%>
<%--news--%>
<%--</span>--%>
<%--                        <h3 class="card-title text-center mt-3">Resume Management and Interview Coordination</h3>--%>
<%--                        <p class="mt-2">Efficient resume management and streamlined interview coordination are critical--%>
<%--                            components of--%>
<%--                            a successful hiring process. Our HRMS platform offers robust features for resume management,--%>
<%--                            ensuring that HR teams can effectively organize, evaluate, and track candidate resumes with--%>
<%--                            ease.</p>--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--            &lt;%&ndash;   <div class="col-lg-4 mb-4 margin_top90">--%>
<%--                   <div class="card shadow-sm">--%>
<%--                       <div class="card-body">--%>
<%--                           <span class="material-symbols-outlined" style="color:blue;">--%>
<%--   room_preferences--%>
<%--   </span>--%>
<%--                           <h3 class="card-title text-center">Empowering Employees: Introducing Self-Service Portal for Seamless Self-Management</h3>--%>
<%--                       </div>--%>
<%--                   </div>--%>
<%--               </div>&ndash;%&gt;--%>
<%--        </div>--%>
<%--    </div>--%>
<%--</section>--%>


<div class="client_section p-5" style="height: auto">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="bottom_heading text-center">Leave Requests</h4>
                <p style="text-align: center">Allowing employees to submit leave requests through an online system or by completing paper forms. Requests should include details such as the type of leave, dates requested, reason for leave, and any supporting documentation if required.</p>
            </div>
        </div>
    </div>
</div>
<!-- footer section start -->
<jsp:include page="footer.jsp" />

<%--jsScript--%>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js">
</script>
<script>
    function hrPlatform() {
        window.open('/hrPlatform','_self');
    }

    function empPlatform(){
        window.open('/employeePlatform','_self')
    }

</script>
</body>
</html>