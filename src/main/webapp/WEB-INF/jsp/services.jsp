!DOCTYPE html>
<html lang="en">
<head>
   <!-- basic -->
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <!-- mobile metas -->
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <meta name="viewport" content="initial-scale=1, maximum-scale=1">
   <!-- site metas -->
   <title>Gallery</title>
   <meta name="keywords" content="">
   <meta name="description" content="">
   <meta name="author" content="">
   <!-- bootstrap css -->
   <link rel="stylesheet" type="text/css" href="..\assets\css\bootstrap.min.css">
   <!-- style css -->
   <link rel="stylesheet" type="text/css" href="..\assets\css\style.css">
   <!-- Responsive-->
   <link rel="stylesheet" href="..\assets\css\responsive.css">
   <!-- fevicon -->
   <link rel="icon" href="..\assets\images\fevicon.png" type="image/gif"/>
   <!-- Scrollbar Custom CSS -->
   <link rel="stylesheet" href="..\assets\css\jquery.mCustomScrollbar.min.css">
   <!-- Tweaks for older IEs-->
   <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
   <!-- owl stylesheets -->
   <link rel="stylesheet" href="..\assets\css\owl.carousel.min.css">
   <link rel="stylesheet" href="..\assets\css\owl.theme.default.min.css">
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css"
         media="screen">
<body>
<!--header section start -->
<div class="header_section">
   <div class="container-fluid">
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
         <div class="logo"><a href="workpunch"><img src="..\assets\images\WorkPunch transparent 1.png"></a></div>
         <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                 aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
         </button>
         <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav  ml-auto">
               <li class="nav-item dropdown dropdown dropdown-cols-2">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                     data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                     Features
                  </a>
                  <div class="dropdown-menu ">
                     <div>
                        <ul>
                           <li>
                              <h3 class="text-center font-weight-bold">HR</h3>
                              <a class="dropdown-item " href="recruitment">Recruitment</a>
                              <a class="dropdown-item" href="#">Onboarding</a>
                              <a class="dropdown-item " href="#">Attendance Management</a>
                              <a class="dropdown-item" href="#">Shift Management</a>
                           </li>
                        </ul>
                     </div>
                     <div>
                        <ul>
                           <li>
                              <h3 class="text-center font-weight-bold">Employee</h3>
                              <a class="dropdown-item " href="#">Attendance Management</a>
                              <a class="dropdown-item" href="#">Shift Management</a>
                              <a class="dropdown-item " href="#">Attendance Management</a>
                              <a class="dropdown-item" href="#">Shift Management</a>
                           </li>
                        </ul>

                     </div>
                  </div>
               </li>
               <%--                     //Solution--%>
               <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                     data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                     Solution
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                     <a class="dropdown-item " href="#">Attendance Management</a>
                     <a class="dropdown-item" href="#">Shift Management</a>
                     <a class="dropdown-item" href="#">TimeSheet</a>
                     <div class="dropdown-divider"></div>
                  </div>
               </li>
               <li class="nav-item">
                  <a class="nav-link" href="#">Pricing</a>
               </li>
               <%--                     Resources--%>
               <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                     data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                     Resources
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                     <a class="dropdown-item" href="#">Administrator Guide</a>
                     <a class="dropdown-item" href="#">Employee Handbook</a>
                     <a class="dropdown-item" href="#">Implimentation Guide</a>
                     <a class="dropdown-item" href="#">API Guide</a>
                     <a class="dropdown-item" href="#">Integration</a>
                     <a class="dropdown-item" href="#">Training</a>
                     <a class="dropdown-item" href="#">Webinars</a>
                     <a class="dropdown-item" href="#">Vedio</a>
                     <a class="dropdown-item" href="#">Become a Partner</a>
                     <div class="text-center w-100">
                        <button class="btn btn-primary" type="button"><a href="/">View</a></button>
                     </div>

                     <div class="dropdown-divider"></div>
                  </div>
               </li>
               <li>
                  <button class="btn btn-danger" type="submit">Get Started</button>
               </li>

            </ul>
         </div>
      </nav>
   </div>
</div>
<!--header section end -->
<!-- banner section start -->
<div class="banner_section layout_padding">
   <div class="container">
      <div class="row">
         <div class="banner_taital">
            <h1 class="text-center font-weight-bold" id="headingName">Great employees don't happen by chance, they are recruited with purpose</h1>
            <%--                           <h1 class="text-center font-weight-bold"  id="headingName"> And Productivity </h1>--%>
            <p class="text-justify text-lg-center">Our recruitment team is dedicated to identifying and attracting top-tier candidates with the skills, experience, and passion that align perfectly with your company's values and objectives. We leverage our extensive network, cutting-edge sourcing techniques, and a meticulous selection process to ensure we present you with only the most qualified candidates.</p>
            <div class=" text-center">
               <div type="button" class="btn btn-danger">Register Today</div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="recruitment_container">
   <div class="container">
      <div class="row" style="height:200px;">
         <div class="col-md-6" style="border:hidden">
            <div>
               <h1 class="recruitment_heading font-weight-bold">
                  Choose wisely, Hire brilliantly
               </h1>
            </div>

         </div>
         <div class="col-md-6" style="border:hidden">
            <p class="recruitment_para">

            </p>
         </div>
      </div>
   </div>
</div>


<!-- services section end -->
<!-- footer section start -->
<div class="footer_section layout_padding">
   <div class="container">
      <div class="row">
         <div class="col-lg-3 col-sm-6">
            <h3 class="useful_text">About</h3>
            <p class="footer_text">consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation u</p>
         </div>
         <div class="col-lg-3 col-sm-6">
            <h3 class="useful_text">Menu</h3>
            <div class="footer_menu">
               <ul>
                  <li><a href="index.html">Home</a></li>
                  <li><a href="about.html">About Us</a></li>
                  <li><a href="gallery.html">Gallery</a></li>
                  <li><a href="services.html">Services</a></li>
                  <li><a href="contact.html">Contact Us</a></li>
               </ul>
            </div>
         </div>
         <div class="col-lg-3 col-sm-6">
            <h1 class="useful_text">Useful Link</h1>
            <p class="dummy_text">Adipiscing Elit, sed do Eiusmod Tempor incididunt </p>
         </div>
         <div class="col-lg-3 col-sm-6">
            <h1 class="useful_text">Contact Us</h1>
            <div class="location_text">
               <ul>
                  <li>
                     <a href="#">
                        <i class="fa fa-map-marker" aria-hidden="true"></i><span class="padding_left_10">Address : Loram Ipusm</span>
                     </a>
                  </li>
                  <li>
                     <a href="#">
                        <i class="fa fa-phone" aria-hidden="true"></i><span class="padding_left_10">Call : +01 1234567890</span>
                     </a>
                  </li>
                  <li>
                     <a href="#">
                        <i class="fa fa-envelope" aria-hidden="true"></i><span class="padding_left_10">Email : demo@gmail.com</span>
                     </a>
                  </li>
               </ul>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- footer section end -->
<!-- copyright section start -->
<div class="copyright_section">
   <div class="container">
      <p class="copyright_text">2020 All Rights Reserved. Design by <a href="https://html.design">Free html  Templates</a></p>
   </div>
</div>
<!-- copyright section end -->
<!-- Javascript files-->
<script src="js/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.bundle.min.js"></script>
<script src="js/jquery-3.0.0.min.js"></script>
<script src="js/plugin.js"></script>
<!-- sidebar -->
<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="js/custom.js"></script>
</body>


</html>