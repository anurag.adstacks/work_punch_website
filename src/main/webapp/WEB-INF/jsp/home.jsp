<!DOCTYPE html>
<html lang="en">
<head>
    <!-- basic -->
    <meta name="description" content="Best attendance management software in India for Accurate, timely, and Secure staff attendance. Schedule a Demo Now. Manage your attendance, salary, PF, ESI, leave requests, Income tax & TDS. Contact us">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- mobile metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="..\assets\images\work-punch-logo.png">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <!-- site metas -->
    <title>Best Attendance Management Software</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- bootstrap css -->
    <link rel="stylesheet" type="text/css" href="..\assets\css\bootstrap.min.css">
    <!-- style css -->
    <link rel="stylesheet" type="text/css" href="..\assets\css\style.css">
    <!-- Responsive-->
    <link rel="stylesheet" href="..\assets\css\responsive.css">
    <!-- fevicon -->
    <link rel="icon" href="..\assets\images\fevicon.png" type="image/gif"/>
    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="..\assets\css\jquery.mCustomScrollbar.min.css">
    <!-- Tweaks for older IEs-->
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
    <!-- owl stylesheets -->
    <link rel="stylesheet" href="..\assets\css\owl.carousel.min.css">
    <%--<link rel="stylesheet" href="..\assets\css\owl.theme.default.min.css">--%>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css"
          media="screen">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0"/>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">

</head>
<body>
<!--header section start -->
<jsp:include page="header.jsp">
    <jsp:param value="active" name="home"/>
</jsp:include>
<!--header section end -->
<!-- banner section start -->
<div class="text-center banner_section layout_padding mt-xl-5">
    <div class="container">
        <div class="row">
            <div class="banner_taital text-center ">
                <h1 class="text-center font-weight-bold mt-lg-5" style="color: #FFFFFF">Drive Employee Engagement And
                    Productivity</h1>
                <p class="text-justify text-lg-center" style="color: #FFFFFF">Our HRMS (Human Resource Management System) is a comprehensive
                    solution designed to streamline and enhance your organization's HR processes. With its user-friendly
                    interface and powerful features, our system empowers HR professionals to efficiently manage employee
                    data, automate routine tasks, and optimize workforce management.</p>
                <div class=" text-center mt-5">
                    <div type="button" class="btn btn-danger"><a target="_blank" href="http://people.work-punch.com/login"
                                                                 style="color:white;font-weight:bold;">Get Started</a>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
<!-- banner section end -->
<!-- about section start -->
<div class="about_section layout_padding">
    <div class="container img-container ">
        <div class="text-center "><img class="shadow-lg p-3 mb-5 bg-white rounded" src="..\assets\images\attendance-img.webp"
                                       class="img-fluid" alt=""></div>
    </div>
</div>


<!-- about section end -->
<!-- gallery section start -->

<div class="row heading_sectionhR">
    <div class="title-container" style="width: 100%">
        <h2 class="section-heading text-center bold">Simplify HR Management with an All-in-One Software Platform.</h2>
        <p class="paragraph-data text-center">Simplify HR, amplify growth. WorkPunch takes care of the administrative
            tasks so you can focus on strategic initiatives and driving your business forward.</p>
        <%--<div class="text-center mt-md-5">
            <button class="btn btn-primary" style="width:150px;height:50px;">Try a Demo</button>
        </div>--%>
    </div>
</div>
<div class="gallery_section layout_padding">
    <div class="container feature-container">
        <div class="row justify-content-center">
            <div class="col-sm-6" style="border:hidden">
                <h3 class="gallery_taital text-center">HR Platform</h3>
                <p class="gallery_text text-center">WorkPunch HRMS is the all-in-one solution for your HR needs,
                    providing
                    efficiency, accuracy, and simplicity. Experience the power of streamlined HR operations and elevate
                    your organization to new heights. Take the leap with WorkPunch HRMS Software and transform your HR
                    department today.
                </p>
                <br/><br/><br/><br/><br/><br/><br/>
                <ul class="text-left mt-5">
                    <li style="font-weight: bold;font-size: 20px;"><span class="material-symbols-outlined"
                                                                         style="color:darkgreen;font-weight: bold;">done</span>
                        Track attendance with ease
                    </li>
                    <li style="font-weight: bold;font-size: 20px;"><span class="material-symbols-outlined"
                                                                         style="color:darkgreen;font-weight: bold">done</span>
                        Schedule shifts effortlessly
                    </li>
                    <li style="font-weight: bold;font-size: 20px;"><span class="material-symbols-outlined"
                                                                         style="color:darkgreen;font-weight: bold">done</span>
                        Track days off efficiently
                    </li>
                    <li style="font-weight: bold;font-size: 20px;"><span class="material-symbols-outlined"
                                                                         style="color:darkgreen;font-weight: bold">done</span>
                        Convert time to Timesheets
                    </li>
                </ul>


            </div>
            <div class="col-md-6" style="border:hidden">
                <div class="text-center"><img src="..\assets\images\hrManagement.webp" class="img-fluid" alt=""
                                              style="mix-blend-mode:multiply;"></div>
            </div>
        </div>
        <br/><br/><br/><br/><br/><br/>

        <div class="row ">
            <div class="col-md-6" style="border:hidden">
                <div class="text-center"><img src="..\assets\images\centralizeEmpDetails.webp" class="img-fluid" alt="">
                </div>
            </div>
            <div class="col-sm-6" style="border:hidden;margin-top: 2%;">
                <h2 class="gallery_taital text-center">Centralize Employee Data</h2>
                <p class="gallery_text text-center">Easily access and manage comprehensive employee information,
                    including personal details, employment history, and performance records, all in one secure and
                    centralized database.</p>
            </div>
        </div>
        <br/><br/><br/><br/><br/><br/>

        <div class="row">
            <div class="col-sm-6" style="border:hidden">
                <h2 class="gallery_taital text-center">Leave And Attendance Management</h2>
                <p class="gallery_text text-center">WorkPunch HRMS is the all-in-one solution for your HR needs,
                    providing efficiency, accuracy, and simplicity. Experience the power of streamlined HR operations
                    and elevate
                    your organization to new heights. Take the leap with WorkPunch HRMS Software and transform your HR
                    department today.
                </p>
            </div>
            <div class="col-md-6" style="border:hidden;margin-top: -2%">
                <div class="text-center"><img src="..\assets\images\attendence-manangemnt.webp " class="img-fluid" alt=""
                                              style="mix-blend-mode:multiply;"></div>
            </div>
        </div>
        <br/><br/><br/><br/><br/><br/>
        <br/><br/>
        <div class="row ">
            <div class="col-md-6" style="border:hidden">
                <div><img src="..\assets\images\featured.webp" class="img-fluid" alt="" style="mix-blend-mode:multiply;">
                </div>
            </div>
            <div class="col-sm-6" style="border:hidden;margin-top: 2%">
                <h2 class="gallery_taital text-center">Employee Self Service Portal</h2>
                <p class="gallery_text text-center">Empower employees with a self-service portal where they can access
                    and update their information, submit leave requests, view pay stubs, and manage their personal data,
                    fostering engagement and reducing administrative tasks.</p>
            </div>
        </div>
        <br/><br/><br/><br/><br/><br/>
        <div class="row">
            <div class="col-sm-6" style="border:hidden">
                <h2 class="gallery_taital text-center">Efficient Recruitment And Onboarding</h2>
                <p class="gallery_text text-center">Streamline your hiring process with integrated recruitment tools,
                    applicant tracking, and seamless onboarding workflows, ensuring a smooth transition for new
                    employees.
                </p>
            </div>
            <div class="col-md-6" style="border:hidden">
                <div class="text-center"><img src="..\assets\images\Onboarding.webp" class="img-fluid" alt=""
                                              style="mix-blend-mode:multiply;"></div>
            </div>
        </div>
        <br/><br/><br/><br/><br/><br/>

    </div>
    <div class="client_section p-5" style="height: auto;margin-top: -100px">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="bottom_heading text-center">Reporting and Analytics</h4>
                <p style="text-align: center">Gain valuable insights into your workforce with our robust reporting and analytics tools. Track attendance trends, analyze productivity metrics, and identify areas for improvement to optimize your operations and drive business growth.
                </p>
            </div>
        </div>
    </div></div>
</div>
<!-- gallery section end -->
<!-- services section start -->


<!-- services section end -->
<!-- testimonial section start -->


<!-- footer section start -->
<jsp:include page="footer.jsp" />

<!-- copyright section end -->
<!-- Javascript files-->
<script src="..\assets\js\jquery.min.js"></script>
<script src="..\assets\js\popper.min.js"></script>
<script src="..\assets\js\bootstrap.bundle.min.js"></script>
<%--<script src="..\assets\js\jquery-3.0.0.min.js"></script>--%>
<script src="..\assets\js\plugin.js"></script>
<!-- sidebar -->
<script src="..\assets\js\jquery.mCustomScrollbar.concat.min.js"></script>
<script src="..\assets\js\custom.js"></script>

<%--<script src="..\assets\js\clickButton.js"></script>--%>
<script>
    function hrPlatform() {
        window.open('/hrPlatform', '_self');
    }

    function empPlatform() {
        window.open('/employeePlatform', '_self')
    }

</script>

</body>
</html>