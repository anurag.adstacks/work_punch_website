<!DOCTYPE html>
<html lang="en">
<head>
    <!-- basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- mobile metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="icon" type="image/png" href="..\assets\images\work-punch-logo.png">
    <!-- site metas -->
    <title>Work Punch </title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- bootstrap css -->
    <link rel="stylesheet" type="text/css" href="..\assets\css\bootstrap.min.css">
    <link rel="stylesheet" href="..\assets\css\owl.carousel.min.css" type="text/css">
    <!-- style css -->
    <link rel="stylesheet" type="text/css" href="..\assets\css\style.css">
    <!-- Responsive-->
    <link rel="stylesheet" href="..\assets\css\responsive.css">
    <!-- fevicon -->
    <link rel="icon" href="..\assets\images\fevicon.png" type="image/gif"/>
    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="..\assets\css\jquery.mCustomScrollbar.min.css">
    <!-- Tweaks for older IEs-->
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
    <!-- owl stylesheets -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css"
          media="screen">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0"/>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">

</head>
<body>
<jsp:include page="header.jsp">
    <jsp:param value="active" name="resources"/>
</jsp:include>
<div class="container justify-content-center">
    <div class="row">
        <div class="col-lg-12">
            <div class="input-group mb-3">
                <input type="text" class="form-control input-text" placeholder="Search...."
                       aria-label="Recipient's username" aria-describedby="basic-addon2">
                <div class="input-group-append">
                    <button class="btn btn-outline-warning btn-lg" type="button">
                        <i class="fa fa-search"></i></button>
                </div>
            </div>
        </div>
    </div>


</div>

<div class="container-fluid gtco-banner-area">
    <div class="container mt-xl-5">
        <div class="row p-xl-5">
            <div class="col-md-6">
                <h1 class="text-left" style="color:darkgreen">Embracing Technology for Productivity your business. </h1>
                <p>Workpunch  is a online platform designed to simplify and streamline human resource
                    management processes for businesses of all sizes. It offers a range of modules and features to
                    centralize employee data, automate HR tasks, and enhance employee engagement. With its user-friendly
                    interface and extensive functionalities, workpunch provides a convenient solution for
                    managing employee information, leave management, attendance tracking, performance management,
                    onboarding, offboarding, reporting, and more. By leveraging the capabilities of this comprehensive
                    HRMS website, businesses can effectively streamline their HR operations, increase productivity, and
                    improve overall organizational efficiency.</p>
                <h1 class="text-left">Overview</h1>
                <ul class="list-group font-weight-bold">
                    <li>1.) Employee SelfService portal</li>
                    <li>2.) Employee Data Management</li>
                    <li>3.) Leave Management</li>
                    <li>4.) Attendance Tracking</li>
                    <li>5.) HR Management</li>
                    <li>6.) Monthly Summary</li>
                </ul>
                <a href="/contactUs">Contact Us <i class="fa fa-angle-right" aria-hidden="true" style="margin-top:40px"></i></a>
            </div>
            <div class="col-md-6">
                <div class="mt-xl-5"style="margin-top:120px!important;"><img class="text-center" src="../assets/images/banner-img.png" alt="">
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container-fluid gtco-features" id="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <h2> Explore The Services<br/>
                    We Offer For You </h2>
                <p>
                    HR Management and employee management are essential components of an organization's success. HR
                    management involves recruiting, hiring, and training employees, managing employee benefits and
                    performance evaluations, and ensuring compliance with labor laws. Effective employee management
                    involves fostering a positive work environment, promoting employee engagement, and supporting career
                    development to maximize productivity and retention.</p>
                </div>

            <div class="col-lg-8">
                <svg id="bg-services"
                     width="100%"
                     viewBox="0 0 1000 800">
                    <defs>
                        <linearGradient id="PSgrad_02" x1="64.279%" x2="0%" y1="76.604%" y2="0%">
                            <stop offset="0%" stop-color="rgb(1,230,248)" stop-opacity="1"/>
                            <stop offset="100%" stop-color="rgb(29,62,222)" stop-opacity="1"/>
                        </linearGradient>
                    </defs>
                    <path fill-rule="evenodd" opacity="0.102" fill="url(#PSgrad_02)"
                          d="M801.878,3.146 L116.381,128.537 C26.052,145.060 -21.235,229.535 9.856,312.073 L159.806,710.157 C184.515,775.753 264.901,810.334 338.020,792.380 L907.021,652.668 C972.912,636.489 1019.383,573.766 1011.301,510.470 L962.013,124.412 C951.950,45.594 881.254,-11.373 801.878,3.146 Z"/>
                </svg>
                <div class="row">
                    <div class="col">
                        <div class="card text-center">
                            <div class="oval"><%--<img class="card-img-top" src="images/web-design.png" alt="">--%></div>
                            <div class="card-body">
                                <h3 class="card-title">HR Management</h3>
                                <p class="card-text">Optimizing Human Capital for Organizational Success.</p>
                            </div>
                        </div>
                        <%--                        <div class="card text-center">--%>
                        <%--                            <div class="oval"><img class="card-img-top" src="images/marketing.png" alt=""></div>--%>
                        <%--                            <div class="card-body">--%>
                        <%--                                <h3 class="card-title"></h3>--%>
                        <%--                                <p class="card-text">Nullam quis libero in lorem accumsan sodales. Nam vel nisi--%>
                        <%--                                    eget.</p>--%>
                        <%--                            </div>--%>
                        <%--                        </div>--%>
                    </div>
                    <div class="col">
                        <div class="card text-center">
                            <div class="oval"><%--<img class="card-img-top" src="images/seo.png" alt="">--%></div>
                            <div class="card-body">
                                <h3 class="card-title">Employee Management</h3>
                                <p class="card-text">Effective Employee Management for Organizational Success</p>
                            </div>
                        </div>
                        <div class="card text-center">
                            <div class="oval"><%--<img class="card-img-top" src="images/graphics-design.png" alt="">--%></div>
                            <div class="card-body">
                                <h3 class="card-title">Administration</h3>
                                <p class="card-text">Efficient and Effective Administration for Optimal Results</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="client_section p-5" style="height: auto;margin-top: 110px">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="bottom_heading text-center">Time and Attendance Software</h4>
                <p style="text-align: center">Invest in time and attendance software that integrates with your HR and payroll systems. These software solutions automate the attendance tracking process, generate reports, and ensure compliance with labor regulations.
                </p>
            </div>
        </div>
    </div>
</div>
<div class="client_section" style="height: 50px;background-color: transparent">
    <%--<div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="bottom_heading text-center">Time and Attendance Software</h4>
                <p style="text-align: center">Invest in time and attendance software that integrates with your HR and payroll systems. These software solutions automate the attendance tracking process, generate reports, and ensure compliance with labor regulations.
                </p>
            </div>
        </div>
    </div>--%>
</div>

<!-- footer section start -->
<jsp:include page="footer.jsp" />


<!-- Optional JavaScript -->
<!-- copyright section end -->
<!-- Javascript files-->
<script src="..\assets\js\jquery.min.js"></script>
<script src="..\assets\js\popper.min.js"></script>
<script src="..\assets\js\bootstrap.bundle.min.js"></script>
<%--<script src="..\assets\js\jquery-3.0.0.min.js"></script>--%>
<script src="..\assets\js\plugin.js"></script>
<!-- sidebar -->
<script src="..\assets\js\jquery.mCustomScrollbar.concat.min.js"></script>
<script src="..\assets\js\custom.js"></script>
<%--<script src="..\assets\js\clickButton.js"></script>--%>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script>
    function hrPlatform() {
        window.open('/hrPlatform', '_self');
    }

    function empPlatform() {
        window.open('/employeePlatform', '_self')
    }

</script>

</body>

</html>
