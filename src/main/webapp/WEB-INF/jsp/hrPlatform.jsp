<!DOCTYPE html>
<html lang="en">
<head>
    <!-- basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- mobile metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="..\assets\images\work-punch-logo.png">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <!-- site metas -->
    <title>HR Platform</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- bootstrap css -->
    <link rel="stylesheet" type="text/css" href="..\assets\css\bootstrap.min.css">
    <!-- style css -->
    <link rel="stylesheet" type="text/css" href="..\assets\css\style.css">
    <!-- Responsive-->
    <link rel="stylesheet" href="..\assets\css\responsive.css">
    <!-- fevicon -->
    <link rel="icon" href="..\assets\images\fevicon.png" type="image/gif"/>
    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="..\assets\css\jquery.mCustomScrollbar.min.css">
    <!-- Tweaks for older IEs-->
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
    <!-- owl stylesheets -->
    <link rel="stylesheet" href="..\assets\css\owl.carousel.min.css">
    <%--<link rel="stylesheet" href="..\assets\css\owl.theme.default.min.css">--%>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css"
          media="screen">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0"/>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">
</head>
<body>
<!--header section start -->
<jsp:include page="header.jsp">
    <jsp:param value="active" name="ourPlatform"/>
    <jsp:param value="active" name="hrPlatform"/>
</jsp:include>

<!--header section end -->
<!-- banner section start -->
<!-- banner section start -->
<div class="banner_section text-center layout_padding ">
    <div class="container">
        <div class="row">
            <div class="banner_taital text-center" style="margin-top: 6%">
                <h1 class="h1 text-center font-weight-bold" id="headingName" style="color: #FFFFFF">Empowering HR Professionals: Transforming
                    People Management through Technology</h1>
                <p class="text-justify text-lg-center" style="color: #FFFFFF">Embracing technology within HR functions empowers HR
                    professionals to elevate their strategic role within organizations. By leveraging the transformative
                    power of technology, HR departments can optimize people management practices, enhance employee
                    experiences, and drive organizational growth in an increasingly digital world.
                </p><br/>
                <div class=" text-center">
                    <div type="button" class="btn btn-danger"><a target="_blank" href="http://people.work-punch.com/login"
                                                                 style="color:white;font-weight:bold">Get Started</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="about_section layout_padding">
    <div class="container hr_img-container ">
        <div class="text-center"><img class="shadow-lg p-3 mb-5 bg-white rounded" id="hr_mainbg" src="..\assets\images\hr_text1.jpg" class="img-fluid" alt=""
                                      style="/*mix-blend-mode:multiply;*/"></div>
    </div>
</div>
<div class="container hr-container ">
    <div class="container-heading ">
        <h2 class="hr_heading text-center mt-lg-5">Empowering HR Excellence: HRMS Solutions for Efficient Workforce
            Management</h2>

        <div class="row row-cols-1 row-cols-md-3 mt-5 g-4 shadow p-3 mb-5 bg-white rounded ">
            <div class="col">
                <div class="card h-100 p-3">
                    <img src="..\assets\images\8252818.png" class="card-img-top"
                         alt="Hollywood Sign on The Hill"/>
                    <div class="card-body">
                        <h5 class="card-title bold"></h5>
                        <p class="card-text text-center">
                            Efficient management of employee data and information.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card h-100 p-3">
                    <img src="..\assets\images\HR Automation black.png" class="card-img-top"
                         alt="Palm Springs Road"/>
                    <div class="card-body">
                        <h5 class="card-title"></h5>
                        <p class="card-text text-center">
                            Streamlined HR processes and automation of administrative tasks.
                        </p>
                    </div>
                </div>
            </div>
<%--            <div class="col">--%>
<%--                <div class="card h-100">--%>
<%--                    <img src="..\assets\images\div3.jpg" class="card-img-top"--%>
<%--                         alt="Los Angeles Skyscrapers"/>--%>
<%--                    <div class="card-body">--%>
<%--                        <h5 class="card-title"></h5>--%>
<%--                        <p class="card-text text-center">Enhanced accuracy and reliability of HR-related activities.</p>--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--            </div>--%>
            <div class="col">
                <div class="card h-100">
                    <img src="..\assets\images\pros.png" class="card-img-top"
                         alt="Skyscrapers"/>
                    <div class="card-body">
                        <h5 class="card-title"></h5>
                        <p class="card-text text-center">
                            Improved employee engagement and self-service capabilities.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="container hr_wholework text-left">
        <h2 class="hrwork_title" style="text-align: center;color: #000000 !important;">Employee Record reveals
            valuable insights into
            workforce demographics, performance, and
            engagement.</h2>
        <div class="row  pt-xl-3 mt-xl-5">
            <div class="col-md-6 mt-lg-5 text-center" style="border:none;margin-top: 6% !important;">
                <h2 class="work_title">Employee Record</h2>
                <p style="margin-left:-1px;">
                    Employee Record in work punch refers to the collection and storage of information related to an
                    employee's attendance and working hours. It typically includes details such as the employee's name,
                    employee ID, date and time of arrival and departure, and any breaks taken during the workday. This
                    data is recorded using a time clock system or electronic attendance tracking software.
                </p>
                    <%--<a  href="*" class=link-info" style="font-size:18px;font-weight:bold;text-decoration: underline;">Learn More</a>--%>
            </div>
            <div class="col-md-6" style="border:none;">
                <div class="text-center shadow p-3 mb-5 bg-white rounded mt-5">
                    <img src="..\assets\images\employeeRecord.webp" alt="">
                </div>

            </div>
        </div><br/>
        <div class="row  pt-xl-5 mt-xl-5">
            <div class="col-md-6" style="border:none;">
                <div class="text-center shadow p-3 mb-5 bg-white rounded mt-5">
                    <img src="..\assets\images\attendanceList.webp" alt="">
                </div>

            </div>
            <div class="col-md-6 text-center pl-5" style="border:none;margin-top: 5%">
                <h2>Attendance Management</h2>
                <p style="margin-left:-1px;">
                    Attendance management in work punch involves the systematic monitoring and control of employee
                    attendance in an organization. It utilizes work punch systems, such as time clocks or electronic
                    attendance tracking software, to record and manage employee attendance data.
                </p>
                <%--<a  href="*" class=link-info" style="font-size:18px;font-weight:bold;text-decoration: underline;">Learn More</a>--%>

            </div>
        </div><br/>
        <div class="row  pt-xl-5 mt-xl-5">
            <div class="col-md-6 mt-lg-5 text-center" style="border:none;margin-top: 5% !important;">
                <h2 class="work_title">Recruitment And Onboarding</h2>
                <p style="margin-left:-1px;">
                    Recruitment and Onboarding" refers to the process of hiring and integrating new employees into an organization. It encompasses various activities aimed at attracting, selecting, hiring, and orienting new employees to ensure a smooth transition into their roles and the company culture
                </p>
                <%--<a  href="*" class=link-info" style="font-size:18px;font-weight:bold;text-decoration: underline;">Learn More</a>--%>

            </div>
            <div class="col-md-6" style="border:none;">
                <div class="text-center shadow p-3 mb-5 bg-white rounded mt-5">
                    <img src="..\assets\images\onboarding.webp" alt="">
                </div>

            </div>
        </div><br/>
        <div class="row  pt-xl-5 mt-xl-5">
            <div class="col-md-6" style="border:none;">
                <div class="text-center shadow p-3 mb-5 bg-white rounded mt-5">
                    <img src="..\assets\images\timeOff.webp" alt="">
                </div>
            </div>
            <div class="col-md-6 text-center pl-5" style="border:none;margin-top: 5%">
                <h2 class="work_title">Leave Management</h2>
                <p style="margin-left:-1px;">
                    Leave management refers to the process of managing and tracking employee absences from work, including various types of leaves such as vacation, sick leave, personal leave, and maternity/paternity leave. The primary goal of leave management is to ensure that employees take leave in accordance with company policies while minimizing disruptions to workflow and maintaining productivity.
                </p>
                <%--<a  href="*" class=link-info" style="font-size:18px;font-weight:bold;text-decoration: underline;">Learn More</a>--%>
            </div>
            </div>

        <div class="row pt-xl-5 mt-xl-5">
            <div class="col-md-6 text-center pl-5" style="border:none;margin-top: 7% !important;">
                <h2 class="work_title">Month Summary</h2>
                <p style="margin-left: -1px;">
                    A monthly summary of an employee's tasks, attendance, leave, and other relevant information provides a comprehensive overview of their performance and activities during a specific month.
                </p>
                <%--<a  href="*" class=link-info" style="font-size:18px;font-weight:bold;text-decoration: underline;">Learn More</a>--%>
            </div>
            <div class="col-md-6" style="border:none;">
                <div class="text-center shadow p-3 mb-5 bg-white rounded mt-5">
                    <img src="..\assets\images\summaryReport.webp" alt="">
                </div>

            </div>
        </div>
    </div>
    <div class="client_section p-5" style="height: auto;margin-top: -100px">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="bottom_heading text-center">Today's actions lay the foundation for a better tomorrow</h4>
                    <p style="text-align: center">Learn how our HR, payroll, and workforce management solutions help you achieve a better work experience for all your people.
                        "Today's actions build tomorrow's success. We offer HR, payroll, and workforce solutions tailored to your needs. Our tools streamline operations and elevate employee experiences. Navigate workplace complexities confidently with our innovative technologies. Join us in shaping the future of work and unlocking your workforce's full potential. Let's create a workplace where success is celebrated.
                    </p>
                </div>
            </div>
        </div></div>
</div>

<%--<div class="row  pt-xl-5 mt-xl-5" style="background-color: #1da0f2;width: 100%;margin-top: -90px !important;height: auto">
    <div class="row" style="margin-left: 6%;">
        <div class="col-md-6"  style="border:none;margin-top: 3%">
            <h3 class="h1 text-center p-5" style="color:white;font-weight:bold;font-size:30px;">Today's actions lay the foundation for a better tomorrow</h3>

            <p class="text-center pl-5" style="color:white;">Learn how our HR, payroll, and workforce management solutions help you achieve a better work experience for all your people.
                "Today's actions build tomorrow's success. We offer HR, payroll, and workforce solutions tailored to your needs. Our tools streamline operations and elevate employee experiences. Navigate workplace complexities confidently with our innovative technologies. Join us in shaping the future of work and unlocking your workforce's full potential. Let's create a workplace where success is celebrated."</p>
            <br/>
            &lt;%&ndash;<button class="btn btn-warning ml-5">Get Started</button>&ndash;%&gt;
        </div>

       &lt;%&ndash; <div class="col-md-6" style="border:none;margin-top: -3%">
            &lt;%&ndash;               <div class="img_div" style="border:2px red solid;">&ndash;%&gt;
            <img class="text-center shadow p-3 mb-5 bg-white rounded mt-5"  id="img-parallel"src="..\assets\images\bg2.jpg" alt="">
        </div>&ndash;%&gt;

    </div>
</div>--%>

<!-- footer section start -->
<jsp:include page="footer.jsp" />

<%--jsScript--%>
<script src="..\assets\js\jquery.min.js"></script>
<script src="..\assets\js\popper.min.js"></script>
<script src="..\assets\js\bootstrap.bundle.min.js"></script>
<%--<script src="..\assets\js\jquery-3.0.0.min.js"></script>--%>
<script src="..\assets\js\plugin.js"></script>
<!-- sidebar -->
<script src="..\assets\js\jquery.mCustomScrollbar.concat.min.js"></script>
<script src="..\assets\js\custom.js"></script>
<%--<script src="..\assets\js\clickButton.js"></script>--%>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>

    //Images changes automatically
    $(document).ready(function () {
        //save boolean
        var pause = false;
        //save items that with number
        var item = $('.select-item');
        //save blocks
        var block = $('.bg-block');
        //variable for counter
        var k = 0;


        //interval function works only when pause is false
        setInterval(function () {
            if (!pause) {
                var $this = item.eq(k);

                if (item.hasClass('active')) {
                    item.removeClass('active');
                }
                ;
                block.removeClass('active').eq(k).addClass('active');
                $this.addClass('active');
                //increase k every 1.5 sec
                k++;
                //if k more then number of blocks on page
                if (k >= block.length) {
                    //rewrite variable to start over
                    k = 0;
                }
            }
            //every 1.5 sec
        }, 3000);


        item.hover(function () {
            //remove active class from all except this
            $(this).siblings().removeClass("active");
            $(this).addClass('active');
            //remove active class from all
            block.removeClass('active');
            //add active class to block item which is accounted as index cliked item
            block.eq($(this).index()).addClass('active');
            //on hover stop interval function
            pause = true;
        }, function () {

            //when hover event ends, start interval function
            pause = false;
        });

    });

//onclick button

    function hrPlatform() {
        window.open('/hrPlatform','_self');
    }

    function empPlatform(){
        window.open('/employeePlatform','_self')
    }
</script>
</body>
</html>
