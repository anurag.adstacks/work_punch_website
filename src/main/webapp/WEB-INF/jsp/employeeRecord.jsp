<!DOCTYPE html>
<html lang="en">
<head>
    <!-- basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- mobile metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="icon" type="image/png" href="..\assets\images\work-punch-logo.png">
    <!-- site metas -->
    <title>Work Punch </title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- bootstrap css -->
    <link rel="stylesheet" type="text/css" href="..\assets\css\bootstrap.min.css">
    <!-- style css -->
    <link rel="stylesheet" type="text/css" href="..\assets\css\style.css">
    <!-- Responsive-->
    <link rel="stylesheet" href="..\assets\css\responsive.css">
    <!-- fevicon -->
    <link rel="icon" href="..\assets\images\fevicon.png" type="image/gif"/>
    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="..\assets\css\jquery.mCustomScrollbar.min.css">
    <!-- Tweaks for older IEs-->
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
    <!-- owl stylesheets -->
    <link rel="stylesheet" href="..\assets\css\owl.carousel.min.css">
    <%--<link rel="stylesheet" href="..\assets\css\owl.theme.default.min.css">--%>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css"
          media="screen">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0"/>



    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">

    <style>
        .material-symbols-outlined {

            font-size: 50px;
        }
    </style>
</head>
<body>
<!--header section start -->
<jsp:include page="header.jsp">
    <jsp:param value="active" name="ourPlatform"/>
    <jsp:param value="active" name="employeeReport"/>
</jsp:include>
<div class="section-heading  p-lg-5 ">
    <div class="row layout_padding">
        <div class="col-md-6">
            <h1 class="text-center">Revolutionize Your Global HR Data Management Experience</h1>
            <p>Revolutionizing your global HR data management experience involves transforming the way you handle and
                utilize HR data across your organization. By implementing innovative technologies and strategies, you
                can streamline processes, enhance data accuracy and security, and empower HR teams with actionable
                insights. </p>
            <p>Centralized Data Management: Implement a centralized HR data management system that consolidates all employee data into a single, integrated platform. This allows for easier access, retrieval, and analysis of HR data across different regions and departments.</p>
        <p>Automation and Digitization: Embrace automation and digitization to streamline routine HR tasks such as employee onboarding, performance reviews, and leave management. This reduces manual effort, minimizes errors, and accelerates processing times.
        </p>
        </div>
        <div class="col-md-6 shadow-sm ">
            <img src="..\assets\images\employeeData.webp" atl="">

        </div>
    </div>
</div>

<div class="header_section mt-xl-1 layout_padding">
    <div class="row p-5 " style="height:auto">
        <div class="col-md-3">
        </div>
        <div class="col-md-12">
            <h1 class="text-center " style="color:green;">Simplify Employee Record Keeping with Database
                Software</h1>
            <p class="text-center">Simplify employee record keeping through
                streamlined
                and user-friendly database software</p>
        </div>

    </div>
</div>


<section id="gallery ">
    <div class="container p-xl-5">
        <div class="row">
            <div class="col-lg-4 mb-4 margin_top90">
                <div class="card shadow-sm">
                    <div class="card-body">
                           <span class="material-symbols-outlined" style="color:green">admin_panel_settings</span>
                        <h5 class="text-left" style="margin-top: -40px;margin-left: 60px">Data Security Measures and Employee Privacy Protection</h5>


                    </div>
                </div>
            </div>
            <div class="col-lg-4 mb-4 margin_top90">
                <div class="card shadow-sm">
                    <div class="card-body">
                        <span class="material-symbols-outlined" style="color:orange;">diversity_3</span>
                        <h5 class="text-left" style="margin-top: -40px;margin-left: 60px">Flexible Workforce Management: Customize and Adapt to Your Requirements</h5>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 mb-4 margin_top90">
                <div class="card shadow-sm">
                    <div class="card-body">
                        <span class="material-symbols-outlined" style="color:blue;">room_preferences</span>
                        <h5 class="text-left" style="margin-top: -40px;margin-left: 60px">Empowering Employees: Introducing Self-Service Portal for Seamless Self-Management</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<div class="client_section p-5" style="height: auto">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="bottom_heading text-center">Define Data Requirements</h4>
                <p style="text-align: center">Determine the types of employee records and information you need to store in the database. This may include personal details, employment history, salary information, performance evaluations, training records, and more. Clearly define data fields and categories to ensure consistency and accuracy in record keeping.
                </p>
            </div>
        </div>
    </div>

</div>

<!-- footer section start -->
<jsp:include page="footer.jsp" />

<%--jsScript--%>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js">
</script>
<script>
    function hrPlatform() {
        window.open('/hrPlatform','_self');
    }

    function empPlatform(){
        window.open('/employeePlatform','_self')
    }

</script>

</body>
</html>