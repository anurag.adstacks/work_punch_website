<!DOCTYPE html>
<html lang="en">
<head>
    <!-- basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- mobile metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="icon" type="image/png" href="..\assets\images\work-punch-logo.png">
    <!-- site metas -->
    <title>Work Punch </title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- bootstrap css -->
    <link rel="stylesheet" type="text/css" href="..\assets\css\bootstrap.min.css">
    <!-- style css -->
    <link rel="stylesheet" type="text/css" href="..\assets\css\style.css">
    <!-- Responsive-->
    <link rel="stylesheet" href="..\assets\css\responsive.css">
    <!-- fevicon -->
    <link rel="icon" href="..\assets\images\fevicon.png" type="image/gif"/>
    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="..\assets\css\jquery.mCustomScrollbar.min.css">
    <!-- Tweaks for older IEs-->
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
    <!-- owl stylesheets -->
    <link rel="stylesheet" href="..\assets\css\owl.carousel.min.css">
    <%--<link rel="stylesheet" href="..\assets\css\owl.theme.default.min.css">--%>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css"
          media="screen">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0"/>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">

</head>
<body>
<!--header section start -->
<jsp:include page="header.jsp">
    <jsp:param value="active" name="ourPlatform"/>
    <jsp:param value="active" name="employeePlatform"/>
</jsp:include>

<div class="banner_section layout_padding ">
    <div class="container">
        <div class="row" style="margin-top: 2%">
            <div class="col-md-6">
                <div class="banner_taital" style="margin-top: 11%">
                    <h1 class="h1  font-weight-bold text-center " id="headingName" style="color: #FFFFFF">Optimize Work Punch Tracking and Reporting
                        with Our Employee Platform</h1>
                    <p class="text-justify " style="color: #FFFFFF">Our employee platform revolutionizes work punch tracking and
                        reporting, delivering optimal efficiency and accuracy to your organization. With our advanced
                        features and intuitive interface, you can effortlessly monitor and manage employee time and
                        attendance. Our platform ensures streamlined data collection, eliminating manual errors and
                        time-consuming processes.
                    </p>
                    <div class="text-center">
                        <div type="button" class="btn btn-danger"><a target="_blank" href="http://people.work-punch.com/login"
                                                                     style="color:white;font-weight:bold">Get
                            Started</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="about_section layout_padding">
                    <div class="container emp_img-container ">
                        <div class="text-center"><img class="shadow-lg p-3 mb-5 bg-white rounded text-center"
                                                      src="..\assets\images\attendanceList.webp" class="img-fluid" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><br><br><br>
<%--<div class="container" style="border:2px green solid;height:200px;">
    <div class="row" style="background-color: lightblue">

    </div>--%>

<%--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">--%>
<div class="">
    <h1 class="text-center">Empowering Employees, Enhancing Work Environment and Culture</h1>
    <p class="text-center mt-3 ">At our organization, we strongly believe in empowering our employees to thrive and
        excel
        in a positive work
        environment.
        We prioritize creating a workplace culture that fosters growth, collaboration, and innovation.
        Our employees are at the heart of our success, and we are dedicated to providing them with the tools, resources,
        and support they need to reach their full potential.</p>


    <div class="row text-center shadow-sm mt-5 p-5">
        <div class="col">
            <div class="card h-100">
                <div class="card-body">
                    <h5 class="card-title">Leave Report <img src="..\assets\images\leaveReport.jpg" alt=""
                                                             style="height:40px;width:40px;"></h5>
                    <p class="card-text">Insights into Employee Leave: Robust Reporting Features</p>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card h-100" >
                <div class="card-body">
                    <h5 class="card-title">Task Management <img src="..\assets\images\taskManagement.png" alt=""
                                                                style="height:40px;width:40px;"></h5>
                    <p class="card-text">Efficiently manage tasks, assignments, and deadlines with our intuitive task
                        management solution</p>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card h-100" >
                <div class="card-body">
                    <h5 class="card-title">Add Notes <img src="..\assets\images\add_notes.png" alt=""
                                                          style="height:40px;width:40px;"></h5>
                    <p class="card-text">Capture important information and insights with our user-friendly note-taking
                        feature, enhancing organization and productivity in one place.</p>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card h-100 " >
                <div class="card-body">
                    <h5 class="card-title">Add Files <img src="..\assets\images\add_files.jpg" alt=""
                                                          style="height:40px;width:40px;"></h5>
                    <p class="card-text">Concise and organized information storage for easy access and reference</p>

                </div>
            </div>
        </div>

    </div>
</div>


<div class="emp_services container-fluid p-xl-5" <%--style="background-color: #F5F5F5"--%>>
    <h1 class="text-center" style="  text-shadow: 2px 2px 4px wheat;">Employee Self Services</h1>
    <div class="row mt-xl-5">
        <div class="col-md-7">
            <h2 class="text-left ml-4 mt-xl-5">Simplified Leave Request Process: Apply for Time Off with Ease</h2>
            <p class="text-left mt-xl-3"> Our platform provides employees with a seamless experience to request time off
                for
                various reasons,
                whether it's for personal matters, vacations, or other commitments. Through our intuitive web employees
                can conveniently submit their leave requests, specify the desired dates, and
                provide any necessary details.</p>
        </div>
        <div class="col-md-4 justify-content-center">
            <div class="text-center shadow-lg">
                <img src="..\assets\images\portalLeaveApplying.webp" alt="">
            </div>
        </div>
    </div>
    <div class="row mt-xl-5">
        <div class="col-md-6 justify-content-center mt-5">
            <div class="text-center shadow-lg">
                <img src="..\assets\images\taskManagement.webp" alt="">
            </div>
        </div>
        <div class="col-md-6 mt-5">
            <h2 class="text-left ml-4 mt-xl-5">Enhance Project Success: Strategic Task Management Approaches</h2>
            <p class="text-left mt-xl-3">Effective task management plays a crucial role in enhancing project success. By
                adopting strategic approaches to task management, organizations can optimize productivity, ensure
                efficient resource allocation, and drive successful project outcomes. </p>
        </div>

    </div>
    <div class="row mt-xl-5">
        <div class="col-md-6">
            <h2 class="text-left ml-4 mt-xl-5">Work Notes and Reflections: Learning from Experiences</h2>
            <p class="text-left mt-xl-3">Work Notes and Reflections provide a valuable opportunity for individuals to
                capture their thoughts, insights, and observations related to their work experiences. By taking the time
                to jot down key learnings, challenges faced, and successes achieved, individuals can gain deeper
                insights into their professional growth and development</p>
        </div>
        <div class="col-md-6 justify-content-center">
            <div class="text-center shadow-lg">
                <img src="..\assets\images\writeNotes.webp" alt="">
            </div>
        </div>
    </div>
    <div class="row mt-xl-5">
        <div class="col-md-6 justify-content-center mt-5">
            <div class="text-center shadow-lg">
                <img src="..\assets\images\fileUpload.webp" alt="">
            </div>
        </div>
        <div class="col-md-6 mt-5">
            <h2 class="text-left ml-4 mt-xl-5">Accelerate Workflows: Swift and Reliable File Uploads</h2>
            <p class="text-left mt-xl-3">Our platform offers swift and reliable file uploads, designed to streamline
                your workflows and boost productivity. With our advanced file upload feature, you can effortlessly
                upload documents, images, and other file types, ensuring efficient information exchange within your
                organization</p>
        </div>

    </div>

</div>
<div class="client_section p-5" style="height: auto">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="bottom_heading text-center">Employee management</h4>
                <p style="text-align: center">Employee management, also known as human resource management (HRM) or personnel management, refers to the processes, strategies, and practices involved in overseeing the workforce within an organization. It encompasses various aspects of managing employees throughout their employment lifecycle, from recruitment and onboarding to performance evaluation and offboarding.
                </p>
            </div>
        </div>
    </div>
</div>



<!-- footer section start -->
<jsp:include page="footer.jsp" />


<%--jsScript--%>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js">
</script>
<script>
    function hrPlatform() {
        window.open('/hrPlatform','_self');
    }

    function empPlatform(){
        window.open('/employeePlatform','_self')
    }

</script>
</body>
</html>
