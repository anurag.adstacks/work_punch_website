<!DOCTYPE html>
<html lang="en">
<head>
    <!-- basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- mobile metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="icon" type="image/png" href="..\assets\images\work-punch-logo.png">
    <!-- site metas -->
    <title>Work Punch </title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- bootstrap css -->
    <link rel="stylesheet" type="text/css" href="..\assets\css\bootstrap.min.css">
    <!-- style css -->
    <link rel="stylesheet" type="text/css" href="..\assets\css\style.css">
    <!-- Responsive-->
    <link rel="stylesheet" href="..\assets\css\responsive.css">
    <!-- fevicon -->
    <link rel="icon" href="..\assets\images\fevicon.png" type="image/gif"/>
    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="..\assets\css\jquery.mCustomScrollbar.min.css">
    <!-- Tweaks for older IEs-->
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
    <!-- owl stylesheets -->
    <link rel="stylesheet" href="..\assets\css\owl.carousel.min.css">
    <%--<link rel="stylesheet" href="..\assets\css\owl.theme.default.min.css">--%>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css"
          media="screen">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0"/>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">

</head>
<body>
<!--header section start -->
<!--header section start -->
<jsp:include page="header.jsp">
    <jsp:param value="active" name="ourPlatform"/>
    <jsp:param value="active" name="taskManagement"/>
</jsp:include>

<div class="banner_container p-xl-5">
    <div class="row mt-xl-5">
        <h1 style="width: 100%;text-align: center">Centralized Task Management: Simplify Work Planning on WorkPunch</h1>
        <p class="text-center">
            WorkPunch offers a powerful project management solution that allows teams to optimize their project
            workflows and streamline task management. With our platform, project managers and teams can effectively
            plan, track, and execute tasks to ensure project success.</p>
    </div>
</div>
<div class="row p-xl-5">
    <div class="col-md-6 p-xl-5">
        <div class="img_cont shadow-lg">
            <img src="../assets/images/taskManagment.jpeg" alt="leaveapply">
        </div>
    </div>
    <div class="col-md-6 mt-xl-5">
        <h2 class="intro_heading text-center mt-xl-5">
            Enhance Team Productivity with Task Management on WorkPunch
        </h2>
        <p class="text-center">WorkPunch provides a robust task management system that aims to enhance team
            productivity. With our platform, teams can streamline their workflow, collaborate effectively, and stay
            organized to achieve optimal results.Efficient task management on WorkPunch enables teams to prioritize,
            assign, and track tasks seamlessly. By centralizing task management within the platform, teams can easily
            access and update task details, ensuring clarity and transparency across all members.</p>
    </div>
</div>

<div class="client_section p-5" style="height: auto">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="bottom_heading text-center">Create a Centralized Workspace</h4>
                <p style="text-align: center">Set up a centralized workspace within your chosen task management tool where all tasks, projects, and related information will be stored and accessed. This workspace should serve as a single source of truth for your team's work, providing easy visibility and accessibility to all members.
                </p>
            </div>
        </div>
    </div>
</div>

<!-- footer section start -->
<jsp:include page="footer.jsp" />



<!-- Javascript files-->
<script src="..\assets\js\jquery.min.js"></script>
<script src="..\assets\js\popper.min.js"></script>
<script src="..\assets\js\bootstrap.bundle.min.js"></script>
<%--<script src="..\assets\js\jquery-3.0.0.min.js"></script>--%>
<script src="..\assets\js\plugin.js"></script>
<!-- sidebar -->
<script src="..\assets\js\jquery.mCustomScrollbar.concat.min.js"></script>
<script src="..\assets\js\custom.js"></script>

<%--<script src="..\assets\js\clickButton.js"></script>--%>
<script>
    function hrPlatform() {
        window.open('/hrPlatform', '_self');
    }

    function empPlatform() {
        window.open('/employeePlatform', '_self')
    }

</script>
</body>
</html>