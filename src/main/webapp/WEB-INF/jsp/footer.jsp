<footer class="footer-section" style="margin-top: -80px;height: auto">
    <div class="footer-content">
        <div class="row">
            <div class="col-xl-4 col-lg-4 p-xl-4" style="margin-top: 5px">
                <div class="footer-widget">
                    <div class="footer-logo">
                        <a href=""><img src="..\assets\images\workpunchlogo.png" class="img-fluid"
                                        alt="logo"></a>
                    </div>
                    <div class="footer-text">
                        <p>
                            <%--WorkPunch is a comprehensive web-based platform that serves as a Human Resource
                            Management System (HRMS).--%> Designed to streamline and automate various HR processes,
                            WorkPunch simplifies and optimizes the management of employee attendance, time tracking,
                            and related tasks for organizations of all sizes.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 p-xl-5 mb-30">
                <div class="footer-widget">
                    <div class="footer-widget-heading w-100">
                        <h3>Useful Links</h3>
                    </div>
                    <ul>
                        <li><a href="/home">Home</a></li>
                        <li><a href="/attendanceManagement">Attendance Management</a></li>
                        <%--<li><a href="/recruitmentAndOnboarding">Recruitment & Onboarding</a></li>--%>
                        <li><a href="/monthlySummary">Monthly Summary</a></li>
                        <li><a href="/taskManagement">Task Management</a></li>
                        <li><a href="/applyingLeave">Leave Applying</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 p-xl-5 mb-30">
                <div class="footer-widget">
                    <div class="footer-widget-heading w-100">
                        <h3>Get In Touch</h3>
                        <p style="margin-left: 0">Gurugram, Haryana 122001</p>
                        <a style="margin-left: 0"><a href="/contactUs">support@work-punch.com</a></a></p>
                    </div>

                </div>
            </div>

        </div>
    </div>
    <div class="container">


    </div>
    </div>
</footer>