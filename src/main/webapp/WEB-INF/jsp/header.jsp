<div class="header_section">
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-light bg-light shadow-sm fixed-top">
            <div class="logo"><a href="workpunch"><img src="..\assets\images\WorkPunch transparent 1.png"></a></div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav mx-auto">
                    <li class="nav-item ${param.home}"><a class="nav-link" href="/home">Home</a></li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle ${param.ourPlatform}" href="#" data-toggle="dropdown">Our platform </a>
                        <ul class="dropdown-menu text-center">
                            <div class="text-center mt-2">
                                <button type="button" onclick="hrPlatform()" class="btn btn-outline-info w-75 ${param.hrPlatform}">HR
                                    Management overview
                                </button>
                            </div>
                            <li><a class="dropdown-item ${param.employeeReport}" href="/employeeRecord">Employee Record</a></li>
                            <li><a class="dropdown-item ${param.attendanceManagement}" href="/attendanceManagement">Attendance
                                Management</a></li>
                            <li><a class="dropdown-item ${param.recruitmentAndOnboarding}" href="/recruitmentAndOnboarding">Recruitment And
                                Onboarding </a></li>
                            <li><a class="dropdown-item ${param.leaveReport}" href="/leaveReport">Leave Report</a></li>
                            <li><a class="dropdown-item ${param.monthlySummary}" href="/monthlySummary">Monthly Summary</a></li>

                            <div class="text-center mt-2">
                                <button type="button" onclick="empPlatform()" class="btn btn-outline-info ${param.employeePlatform}">Employee
                                    Management overview
                                </button>
                            </div>
                            <li><a class="dropdown-item ${param.leaveApplying}" href="/applyingLeave">Leave Applying</a></li>
                            <li><a class="dropdown-item ${param.taskManagement}" href="/taskManagement">Task Management</a></li>
                            <li><a class="dropdown-item ${param.addNotes}" href="/addNotes">Add Notes </a></li>
                            <li><a class="dropdown-item ${param.addFiles}" href="/addFiles">Add Files</a></li>


                        </ul>
                    </li>
                    <li class="nav-item ${param.resources}"><a class="nav-link" href="/resources">Resources </a></li>
                    <li class="nav-item ${param.contactUs}"><a class="nav-link" href="/contactUs">Contact Us</a></li>
                </ul>

                <li id="startedBtn">
                    <button class="btn btn-danger" type="submit"><a target="_blank" href="http://people.work-punch.com/login"
                                                                     style="color:white;font-weight:bold;margin: 0 auto">Get
                        Started</a></button>
                </li>
            </div>
        </nav>

    </div>
</div>